# Spark Training

This repository contains a files to spark training and tutorials using Python API (pyspark) and Scala API.

## Pre requisites
- Poetry for Python
- using Pytest and chispa
- sbt for Scala

## Folders
These scripts are separeted by language and into each folder we have `functions` folder to insert functions to be tested, `tests` folder to we create an unit tests and `scripts`folder to be saved all scripts that we see in training.

## About this repository
This repository is to you learn and use without limits, feel free to share with communities and your friends. 

## Under Construction
These tests and tutorials are under constructions but feel free to send me feedback. 


## References
* https://github.com/MrPowers/chispa
* https://mungingdata.com/pyspark/poetry-dependency-management-wheel/
* https://mungingdata.com/pyspark/testing-pytest-chispa/
* https://mrpowers.medium.com/creating-a-pyspark-project-with-pytest-pyenv-and-egg-files-d2709eb1604c

## About author

Hi my name is Diogo Miyake, but you be call me only Miyake, I am a technology enthusiast and Data Platform Engineer, I love development, Open Source tools and these techonologies: programming languages like *Rust* and *Elixir*, *DevOps*, *DevSecOps*, *WoodWorking*, *Cooking*, *Drawing Sketches* and *Painting*, and tolerate other languages, haskell I don't like. I like help people to learning more about dverses themes.  

Fell free to send me a message at [Linkedin](https://www.linkedin.com/in/diogo-miyake/) or [email](akio.miyake@live.com).  
