import pyspark.sql.functions as F

def parse_kafka_data_withoutSchema(spark, dataframe):
    kafka_dataframe = spark.read.json(dataframe.rdd.map(lambda r: r.value))
    return kafka_dataframe

def parse_kafka_data_withSchema(spark, dataframe, schema):
    kafka_dataframe = spark.read.schema(schema).json(dataframe.rdd.map(lambda r: r.value))
    return kafka_dataframe