import pytest

from Python.functions.transformations_finished import parse_kafka_data_withSchema, parse_kafka_data_withoutSchema
from chispa.dataframe_comparer import assert_df_equality
import pyspark.sql.functions as F

from tests.mocked_data import kafka_data, kafka_schema, kafka_expected_schema, kafka_expected_data, kafka_expeted_schema_v2


def test_parse_kafka_data_error(spark):
    

    kafka_dataframe = spark.createDataFrame(
     kafka_data,
     kafka_schema)

    kafka_dataframe = parse_kafka_data_withoutSchema(spark, kafka_dataframe)

    expected_kafka_dataframe = spark.createDataFrame(kafka_expected_data,kafka_expected_schema)


    assert_df_equality(kafka_dataframe, expected_kafka_dataframe, ignore_nullable=True, transforms=None, allow_nan_equality=True, ignore_column_order=True, ignore_row_order=False)

def test_parse_kafka_data(spark):

    kafka_dataframe = spark.createDataFrame(
     kafka_data,
     kafka_schema)

    kafka_dataframe = parse_kafka_data_withSchema(spark, kafka_dataframe, kafka_expeted_schema_v2)

    # columns and schema for expected columns for dataframe
    expected_dataframe = spark.createDataFrame([
        (12,"null","2021-08-22T09:45:59","Residencial",20, 
                "2021-09-20T00:00:00",
                "2021-09-21T00:00:00", 666, 
                ["12345-6", 666, 1234, "CARTAO FL", 1, "Bandeirado"] ,
                [20, "2021-08-22T05:21:25.513Z","2021-08-22T07:21:25.668Z","2021-08-22T05:21:20.777Z",242, "BANCO_FL", "upd"],
                "foi",0),
        (132, "null", "2021-08-21T09:45:59", "Residencial", 20,
                    "2021-09-20T00:00:00", 
                    "2021-09-21T00:00:00", 667,
            ["12345-8", 667, 12345, "CARTAO FL", 1, "Bandeirado"],
            [10, "2021-08-21T06:21:25.513Z","2021-08-21T07:20:25.668Z","2021-08-22T05:21:20.777Z", 242, "BANCO_FL", "upd"],
            "indo", 1)],kafka_expeted_schema_v2)


    assert_df_equality(kafka_dataframe, expected_dataframe,ignore_nullable=True, transforms=None, allow_nan_equality=True, ignore_column_order=True, ignore_row_order=False)