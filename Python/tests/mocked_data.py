# These module contains data mocked to are used in examples
from pyspark.sql.types import StructType, IntegerType, StringType,TimestampType,BooleanType,DoubleType,StructField,LongType,ArrayType,MapType 
from datetime import datetime

kafka_data = [('''{"account_id":12,"person_id":666,"status_id":0,"status_description":"foi","create_date":"2021-08-22T09:45:59","due_day":20,"delivery_address":"Residencial","next_due_date":"2021-09-20T00:00:00","next_real_due_date":"2021-09-21T00:00:00","charge_date":"null","properties":{"cmd_seq":20,"dt_capture":"2021-08-22T05:21:25.513Z","dt_publish":"2021-08-22T07:21:25.668Z","dt_transaction":"2021-08-22T05:21:20.777Z","issuer_id":242,"issuer_name":"BANCO_FL","operation":"upd"},"product":{"product_id":1,"product_description":"CARTAO FL","product_type":"Bandeirado","issuer_bank_number":666,"issuer_branch_number":"1234","issuer_account_number":"12345-6"}}''', '2021-08-22T05:21:25.513Z'),
      ('''{"account_id":132,"person_id":667,"status_id":1,"status_description":"indo","create_date":"2021-08-21T09:45:59","due_day":20,"delivery_address":"Residencial","next_due_date":"2021-09-20T00:00:00","next_real_due_date":"2021-09-21T00:00:00","charge_date":"null","properties":{"cmd_seq":10,"dt_capture":"2021-08-21T06:21:25.513Z","dt_publish":"2021-08-21T07:20:25.668Z","dt_transaction":"2021-08-22T05:21:20.777Z","issuer_id":242,"issuer_name":"BANCO_FL","operation":"upd"},"product":{"product_id":1,"product_description":"CARTAO FL","product_type":"Bandeirado","issuer_bank_number":667,"issuer_branch_number":"12345","issuer_account_number":"12345-8"}}''', '2021-08-21T05:21:25.513Z')]

kafka_schema = ['value', 'timestamp']

kafka_expected_data = [
        (12,"null",datetime.strptime("2021-08-22 09:45:59", "%Y-%m-%d %H:%M:%S"),"Residencial",20, 
                datetime.strptime("2021-09-20 00:00:00", "%Y-%m-%d %H:%M:%S"),
                datetime.strptime("2021-09-21 00:00:00", "%Y-%m-%d %H:%M:%S"), 666, 
                ["12345-6", 666, 1234, "CARTAO FL", 1, "Bandeirado"] ,
                [20, datetime.strptime("2021-08-22 05:21:25.513", "%Y-%m-%d %H:%M:%S.%f"), 
                    datetime.strptime("2021-08-22 07:21:25.668", "%Y-%m-%d %H:%M:%S.%f"), 
                    datetime.strptime("2021-08-22 05:21:20.777", "%Y-%m-%d %H:%M:%S.%f"), 
                242, "BANCO_FL", "upd"],
                "foi",0),
        (132, "null", datetime.strptime("2021-08-21 09:45:59", "%Y-%m-%d %H:%M:%S"), "Residencial", 20,
                    datetime.strptime("2021-09-20 00:00:00", "%Y-%m-%d %H:%M:%S"), 
                    datetime.strptime("2021-09-21 00:00:00", "%Y-%m-%d %H:%M:%S"), 667,
            ["12345-8", 667, 12345, "CARTAO FL", 1, "Bandeirado"],
            [10, datetime.strptime("2021-08-21 06:21:25.513", "%Y-%m-%d %H:%M:%S.%f"), 
                datetime.strptime("2021-08-21 07:20:25.668", "%Y-%m-%d %H:%M:%S.%f"), 
                datetime.strptime("2021-08-22 05:21:20.777", "%Y-%m-%d %H:%M:%S.%f"), 242, "BANCO_FL", "upd"],
            "indo", 1)]

kafka_expected_schema = expected_schema = StructType(
    StructType(
        [
            StructField("account_id",LongType(),True),
            StructField("charge_date",StringType(),True),
            StructField("create_date",TimestampType(),True),
            StructField("delivery_address",StringType(),True),
            StructField("due_day",LongType(),True),
            StructField("next_due_date",TimestampType(),True),
            StructField("next_real_due_date",TimestampType(),True),
            StructField("person_id",LongType(),True),
            StructField("product",StructType(
                [
                    StructField("issuer_account_number",StringType(),True),
                    StructField("issuer_bank_number",LongType(),True),
                    StructField("issuer_branch_number",StringType(),True),
                    StructField("product_description",StringType(),True),
                    StructField("product_id",LongType(),True),
                    StructField("product_type",StringType(),True)]),True),
            StructField("properties",StructType(
                [
                    StructField("cmd_seq",LongType(),True),
                    StructField("dt_capture",TimestampType(),True),
                    StructField("dt_publish",TimestampType(),True),
                    StructField("dt_transaction",TimestampType(),True),
                    StructField("issuer_id",LongType(),True),
                    StructField("issuer_name",StringType(),True),
                    StructField("operation",StringType(),True)]),True),
                    StructField("status_description",StringType(),True),
                    StructField("status_id",LongType(),True)
                    ]))
kafka_expeted_schema_v2 = StructType(
        StructType([
            StructField("account_id",LongType(),True),
            StructField("charge_date",StringType(),True),
            StructField("create_date",StringType(),True),
            StructField("delivery_address",StringType(),True),
            StructField("due_day",LongType(),True),
            StructField("next_due_date",StringType(),True),
            StructField("next_real_due_date",StringType(),True),
            StructField("person_id",LongType(),True),
            StructField("product",StructType(
                [
                    StructField("issuer_account_number",StringType(),True),
                    StructField("issuer_bank_number",LongType(),True),
                    StructField("issuer_branch_number",StringType(),True),
                    StructField("product_description",StringType(),True),
                    StructField("product_id",LongType(),True),
                    StructField("product_type",StringType(),True)]),True),
            StructField("properties",StructType(
                [
                    StructField("cmd_seq",LongType(),True),
                    StructField("dt_capture",StringType(),True),
                    StructField("dt_publish",StringType(),True),
                    StructField("dt_transaction",StringType(),True),
                    StructField("issuer_id",LongType(),True),
                    StructField("issuer_name",StringType(),True),
                    StructField("operation",StringType(),True)]),True),
                    StructField("status_description",StringType(),True),
                    StructField("status_id",LongType(),True)
                    ]))
# Obs.: For debezium we need only payload that are located in =>> "payload":{"after": .. 
## These simulated a CDC for mongodb 
debezium_json = [('''{"schema":{"type":"struct","fields":[{"type":"string","optional":true,"name":"io.debezium.data.Json","version":1,"field":"after"},{"type":"string","optional":true,"name":"io.debezium.data.Json","version":1,"field":"patch"},{"type":"string","optional":true,"name":"io.debezium.data.Json","version":1,"field":"filter"},{"type":"struct","fields":[{"type":"string","optional":false,"field":"version"},{"type":"string","optional":false,"field":"connector"},{"type":"string","optional":false,"field":"name"},{"type":"int64","optional":false,"field":"ts_ms"},{"type":"string","optional":true,"name":"io.debezium.data.Enum","version":1,"parameters":{"allowed":"true,last,false"},"default":"false","field":"snapshot"},{"type":"string","optional":false,"field":"db"},{"type":"string","optional":false,"field":"rs"},{"type":"string","optional":false,"field":"collection"},{"type":"int32","optional":false,"field":"ord"},{"type":"int64","optional":true,"field":"h"},{"type":"int64","optional":true,"field":"tord"},{"type":"string","optional":true,"field":"stxnid"}],"optional":false,"name":"io.debezium.connector.mongo.Source","field":"source"},{"type":"string","optional":true,"field":"op"},{"type":"int64","optional":true,"field":"ts_ms"},{"type":"struct","fields":[{"type":"string","optional":false,"field":"id"},{"type":"int64","optional":false,"field":"total_order"},{"type":"int64","optional":false,"field":"data_collection_order"}],"optional":true,"field":"transaction"}],"optional":false,"name":"TOPICO.MEUDOCUMENTOMONGODB.MinhaColection.Envelope"},"payload":{"after":"{\"_id\": {\"$binary\": \"j7I4T99LAE+VTM6FyDgogw==\",\"$type\": \"03\"},\"creationDate\": {\"$date\": 1608060394087},\"brand\": \"BANCO FL\",\"payload\": \"[{\\\"TradingAccount\\\":12345,\\\"Limit\\\":666,\\\"OverLimit\\\":1,\\\"IsVip\\\":false}]\",\"errors\": [],\"total\": 0,\"totalErrors\": 0}","patch":null,"filter":null,"source":{"version":"1.4.0.Final","connector":"mongodb","name":"topico","ts_ms":1613166859000,"snapshot":"true","db":"MEUDOCUMENTOMONGODB","rs":"rs0","collection":"MinhaColection","ord":6,"h":2373401489743955995,"tord":null,"stxnid":null},"op":"r","ts_ms":1613167035803,"transaction":null}}''', '2021-08-22T05:21:25.513Z'),
('''{"schema":{"type":"struct","fields":[{"type":"string","optional":true,"name":"io.debezium.data.Json","version":1,"field":"after"},{"type":"string","optional":true,"name":"io.debezium.data.Json","version":1,"field":"patch"},{"type":"string","optional":true,"name":"io.debezium.data.Json","version":1,"field":"filter"},{"type":"struct","fields":[{"type":"string","optional":false,"field":"version"},{"type":"string","optional":false,"field":"connector"},{"type":"string","optional":false,"field":"name"},{"type":"int64","optional":false,"field":"ts_ms"},{"type":"string","optional":true,"name":"io.debezium.data.Enum","version":1,"parameters":{"allowed":"true,last,false"},"default":"false","field":"snapshot"},{"type":"string","optional":false,"field":"db"},{"type":"string","optional":false,"field":"rs"},{"type":"string","optional":false,"field":"collection"},{"type":"int32","optional":false,"field":"ord"},{"type":"int64","optional":true,"field":"h"},{"type":"int64","optional":true,"field":"tord"},{"type":"string","optional":true,"field":"stxnid"}],"optional":false,"name":"io.debezium.connector.mongo.Source","field":"source"},{"type":"string","optional":true,"field":"op"},{"type":"int64","optional":true,"field":"ts_ms"},{"type":"struct","fields":[{"type":"string","optional":false,"field":"id"},{"type":"int64","optional":false,"field":"total_order"},{"type":"int64","optional":false,"field":"data_collection_order"}],"optional":true,"field":"transaction"}],"optional":false,"name":"TOPICO.MEUDOCUMENTOMONGODB.MinhaColection.Envelope"},"payload":{"after":"{\"_id\": {\"$binary\": \"j7I4T99LAE+VTM6FyDgogw==\",\"$type\": \"03\"},\"creationDate\": {\"$date\": 1608060394099},\"brand\": \"BANCO FL\",\"payload\": \"[{\\\"TradingAccount\\\":12344,\\\"Limit\\\":777,\\\"OverLimit\\\":1,\\\"IsVip\\\":false}]\",\"errors\": [],\"total\": 0,\"totalErrors\": 0}","patch":null,"filter":null,"source":{"version":"1.4.0.Final","connector":"mongodb","name":"topico","ts_ms":1613167960000,"snapshot":"true","db":"MEUDOCUMENTOMONGODB","rs":"rs0","collection":"MinhaColection","ord":6,"h":2373401489744055995,"tord":null,"stxnid":null},"op":"r","ts_ms":1613167035944,"transaction":null}}''', '2021-08-22T05:22:25.513Z')]


## Example below of data are needed to be converted schema types

data_convert_schema = [
    ("caneta", "1.5", 1.5),
    ("borracha", "0,65", 0.65),
    ("lapis", "1", 1.0),
    ("caderno", "3", 3.0)
]

data_convert_schema_columns = ["product", "price","expected_price"]