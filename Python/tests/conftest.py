import pytest
from pyspark.sql import SparkSession

# these session are necessary to we always get this session to use tests
@pytest.fixture(scope='session')
def spark():
    return SparkSession.builder \
    .master("local") \
    .appName("pyspark_training") \
    .getOrCreate()