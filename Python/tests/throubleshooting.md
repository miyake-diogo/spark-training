# Intro 

The premises of TDD are make simple and little tests and grow continuously.  
For these example, when I run a test like `


Consider the example for transformations of kafka input function, see output rows below... 

How are difference?
```
Row(account_id=12, charge_date='null', create_date='2021-08-22T09:45:59', delivery_address='Residencial', due_day=20, next_due_date='2021-09-20T00:00:00', next_real_due_date='2021-09-21T00:00:00', person_id=666, product=Row(issuer_account_number='12345-6', issuer_bank_number=666, issuer_branch_number='1234', product_description='CARTAO FL', product_id=1, product_type='Bandeirado'), properties=Row(cmd_seq=20, dt_capture='2021-08-22T05:21:25.513Z', dt_publish='2021-08-22T07:21:25.668Z', dt_transaction='2021-08-22T05:21:20.777Z', issuer_id=242, issuer_name='BANCO_FL', operation='upd'), status_description='foi', status_id=0)

Row(account_id=12, charge_date='null', create_date='2021-08-22 09:45:59', delivery_address='Residencial', due_day=20, next_due_date='2021-09-20 00:00:00', next_real_due_date='2021-09-21 00:00:00', person_id=666, product=Row(issuer_account_number='12345-6', issuer_bank_number=666, issuer_branch_number='1234', product_description='CARTAO FL', product_id=1, product_type='Bandeirado'), properties=Row(cmd_seq=20, dt_capture='2021-08-22 05:21:25.513', dt_publish='2021-08-22 07:21:25.668', dt_transaction='2021-08-22 05:21:20.777', issuer_id=242, issuer_name='BANCO_FL', operation='upd'), status_description='foi', status_id=0)


Row(account_id=132, charge_date='null', create_date='2021-08-21T09:45:59', delivery_address='Residencial', due_day=20, next_due_date='2021-09-20T00:00:00', next_real_due_date='2021-09-21T00:00:00', person_id=667, product=Row(issuer_account_number='12345-8', issuer_bank_number=667, issuer_branch_number='12345', product_description='CARTAO FL', product_id=1, product_type='Bandeirado'), properties=Row(cmd_seq=10, dt_capture='2021-08-21T06:21:25.513Z', dt_publish='2021-08-21T07:20:25.668Z', dt_transaction='2021-08-22T05:21:20.777Z', issuer_id=242, issuer_name='BANCO_FL', operation='upd'), status_description='indo', status_id=1)

Row(account_id=132, charge_date='null', create_date='2021-08-21 09:45:59', delivery_address='Residencial', due_day=20, next_due_date='2021-09-20 00:00:00', next_real_due_date='2021-09-21 00:00:00', person_id=667, product=Row(issuer_account_number='12345-8', issuer_bank_number=667, issuer_branch_number='12345', product_description='CARTAO FL', product_id=1, product_type='Bandeirado'), properties=Row(cmd_seq=10, dt_capture='2021-08-21 06:21:25.513', dt_publish='2021-08-21 07:20:25.668', dt_transaction='2021-08-22 05:21:20.777', issuer_id=242, issuer_name='BANCO_FL', operation='upd'), status_description='indo', status_id=1)

```
Are more difficult to see alright, if you use vs code you can see comparison about they and find difference, compare `output_df1.txt` with `output_df2.txt` you see how are de changes of files. 

![image](../images/diff_outputs.png)

You can see in some lines for timestamp columns we not have T and Z for timestamp .. and we can solve it now.. 

